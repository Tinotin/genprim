/// RUST code: Genera números de la primitiva en varias series 

use rand::Rng;

fn main() {

println!("\tRUST code: Genera números de la primitiva en varias series");
println!("\t__________________________________________________________\n");
    for num_serie in 1..50 {
    print!("Serie {:?}: \t", num_serie) ;
        random_primitiva();
    }   
}

fn random_primitiva() {
 // Inicializar la variable para los valores generados por el crate rand
    let mut serie: Vec<u8> = Vec::new();
 //   let mut j: usize = 0;
 // Inicializacion vector de numeros de primitiva
    let num = vec![0;6];
    for _i in &num { 
           
        loop {
            let num_primitiva = new_num();
    //        println!("Ha salido el numero {}", num_primitiva);
            if serie.contains(&num_primitiva) {
 //           println!("Se ha repetido el Numero en le intento de introducirlo en la serie: {}", num_primitiva);
 //               println!("El actual vector tiene el valor: {:?}", serie);
//             let num_primitiva = new_num();
               continue;
            } else {
            
            serie.push(num_primitiva);
            break;
            }       
        }
        serie.sort();
        
   //     print!("\t{} ", serie[i]);
    }
//        println!("\t{:?}", serie);
        for i in 0..6 {
          print!("\t{ser}", ser = serie[i]);
    }

        println!(); // Este salto de linea es para separar cada serie
 }   

/* fn test_intervalos(num_primitiva: u8) {

// Comprobacion que los numeros estan comprendidos entre 1 y 49

if  num_primitiva >= 50 || num_primitiva < 1  {
        println!("Ha salido un valor no comprendido entre  1 y 49: {:?}", num_primitiva);
 }    
}
*/ 

fn new_num() -> u8{ 
    let mut rng = rand::thread_rng();
    let num: u8= rng.gen_range(1..50);
    num
}

